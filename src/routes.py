import time

from bottle import request, abort
from geventwebsocket import WebSocketError

from app import app
from cache import cache
from log import logger


def get_temperature():
    """Gets the temperature."""
    temp = cache.get('temperature')
    if temp:
        return temp, 1

    return 'Unknown temperature', 5


@app.get('/temperature')
def stream_temperature():
    wsocket = request.environ.get('wsgi.websocket')
    if not wsocket:
        abort(400, 'Expected websocket request')

    while True:
        try:
            temp, sleep = get_temperature()
            wsocket.send(temp)
            time.sleep(sleep)
        except WebSocketError as e:
            logger.warn(f'wsocket error: {e}')
            break
        except Exception as e:
            logger.error(f'Unkown error: {e}')
            break


@app.get('/echo')
def echo_stream():
    wsocket = request.environ.get('wsgi.websocket')
    if not wsocket:
        abort(400, 'Expected a websocket request')
    while True:
        try:
            data = wsocket.receive()
            wsocket.send(data)
        except WebSocketError as e:
            logger.warn(f'wsocket error: {e}')
            break
