from bottle import Bottle


app = Bottle()

# import routes
import routes  # noqa

# import callbacks
import callbacks  # noqa
