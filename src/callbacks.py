import json

import pahotoolkit as mqtt
import settings

from log import logger
from cache import cache
from models import Temperature, StreamRequest


@mqtt.subscribe(f'/{settings.MQTT_INTERNAL_NAMESPACE}/temperature')
@mqtt.json_message()
def listen_for_temperature(payload: dict, *args, **kwargs):
    """Handle incoming temperature measure."""
    logger.debug(f'Event: {payload}')
    try:
        temperature = Temperature(payload.get('data'))
    except Exception as e:
        logger.warn(f'Invalid value: {e}')
        return

    temp = temperature.as_dict()
    temp = json.dumps(temp)

    try:
        cache.set('temperature', temp)  # set the latest value
    except Exception as e:
        logger.error(f'Failed to save: {e}')


@mqtt.on_connect()
def request_streams(client, *args, **kwargs):
    streams_topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/streams'
    stream_request = StreamRequest(uri='/temperature',
                                   state=True,
                                   addresses=(None, None))
    client.publish(streams_topic,
                   json.dumps(stream_request.as_dict()))
