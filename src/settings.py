import os


HOSTNAME = os.getenv('HOSTNAME')

PORT = int(os.getenv('PORT', 80))

MQTT_HOST = os.getenv('MQTT_HOST')

MQTT_USERNAME = os.getenv('MQTT_USER')

MQTT_PASSWORD = os.getenv('MQTT_PASSWORD')

MQTT_NAMESPACE = os.getenv('MQTT_NAMESPACE')

MQTT_INTERNAL_NAMESPACE = os.getenv('MQTT_INTERNAL_NAMESPACE')

MQTT_PROTOCOL = os.getenv('MQTT_PROTOCOL')

MQTT_PORT = int(os.getenv('MQTT_PORT', 1883))

LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')

REDIS_HOST = os.getenv('REDIS_HOST')

REDIS_PORT = int(os.getenv('REDIS_PORT'))
