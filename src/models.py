from dataclasses import dataclass, asdict
from datetime import datetime


@dataclass
class Temperature:
    """Temperature Model."""
    value: int
    timestamp: int = 0

    def __post_init__(self):
        if not self.timestamp:
            self.timestamp = datetime.now().timestamp()

    def as_dict(self):
        return asdict(self)


@dataclass
class StreamRequest:
    """Request for streams in the MQTT channel."""

    addresses: (str, str)
    uri: str
    state: bool
    method: str = 'GET'

    def __post_init__(self):
        if len(self.addresses) == 0:
            raise ValueError('At least one address should be included.')

    def as_dict(self):
        return asdict(self)
