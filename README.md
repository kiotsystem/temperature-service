# Temperature Service

[![Build Status](https://ci.moreandcoffee.com/api/badges/kiotsystem/temperature-service/status.svg?branch=master)](https://ci.moreandcoffee.com)

## Contact

- Arnulfo Solis
- arnulfojr94@gmail.com

## License

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)

