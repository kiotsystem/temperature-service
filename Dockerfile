FROM kiotsystem/gevent-websocket-arm:0.10.1

WORKDIR /app

COPY ./requirements.txt ./requirements.txt

RUN pip install -r ./requirements.txt

ENV PYTHONPATH "/app/src"

COPY ./src/ ./src/
COPY ./run.py ./run.py

ENTRYPOINT ["/usr/local/bin/python"]

CMD ["/app/run.py"]
