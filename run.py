import pahotoolkit

from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler

import settings

from app import app
from log import logger


server = WSGIServer((settings.HOSTNAME, settings.PORT),
                    app,
                    handler_class=WebSocketHandler)


if __name__ == '__main__':
    pahotoolkit.start_async(host=settings.MQTT_HOST, port=settings.MQTT_PORT,
                            username=settings.MQTT_USERNAME,
                            password=settings.MQTT_PASSWORD)
    try:
        server.serve_forever()
    finally:  # clean up
        logger.info('Cleaning up')
        pahotoolkit.stop_async()
